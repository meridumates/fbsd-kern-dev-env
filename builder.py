#!/usr/bin/env python
import multiprocessing
import argparse
import os
import shutil

BASE_PATH = None
CORES = 0

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-k", "--kefi", help="build the full KEFI kernel",
                        action="store_true")
    parser.add_argument("-i", "--install", help="install the kernel after " \
    "building", action="store_true")

    return parser.parse_args()

def build_kefi(install=False):
    """
    Builds the entire KEFI kernel
    """
    # base path for navigating checkouts
    global BASE_PATH
    fbsd_src = os.path.join(BASE_PATH, "freebsd")
    # temporary build storage used for building the kernel
    tmp_store = "/tmp/"
    # KEFI src.conf file
    src_conf = "src.conf"
    # Number of cores to build with
    global CORES
    # Directory name in which the kernel will be installed
    kern_name = "KEFI"
    # configuration name for the current build (maps to config file in
    # /sys/amd64/conf)
    kern_conf = "KEFI"

    print("Building KEFI")

    # cd into FreeBSD directory
    os.chdir(fbsd_src)

    # build the kernel
    os.environ["SRCCONF"] = os.path.join(BASE_PATH, "kefi", "build", "src.conf")
    os.environ["MAKEOBJDIRPREFIX"]="/tmp"
    build_cmd = "make buildkernel -j %s" % (CORES)
    os.system(build_cmd)

    if install:
        os.environ["INSTKERNNAME"]="KEFI"
        install_cmd = "make installkernel -j %s" % (CORES)
        os.system(install_cmd)

        # Copy the symbol files into the boot diretory for easy access
        src = "/usr/lib/debug/boot/%s" % (kern_name)
        dst = "/boot/%s/%s_symbols" % (kern_name, kern_name)

        shutil.copytree(src, dst)

def config_builder(args):
    """
    Configures the build script
    """
    global CORES
    global BASE_PATH

    CORES = multiprocessing.cpu_count()
    BASE_PATH = os.path.abspath("../../")

    if args.install:
        if os.geteuid() != 0:
            exit("ERROR: root privileges required to install the kernel")

def run():
    """
    Main function
    """
    args = parse_args()
    config_builder(args)

    if args.kefi:
        build_kefi(args.install)


if __name__ == "__main__":
    run()
