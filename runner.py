#!/usr/bin/env python
import os
import sys
import argparse

VM_NAME = "KEFI"
NUM_VCPU = "1"
MEM = "2G"
CONS = "stdio"
DISK_PATH = "../../images/eval.img"
KERNEL_PATH = "/boot/"
KGDB_PORT = "w1234"
Q_MNT_PATH = "/mnt/q_kefi"
KEFI_KERN_PATH = "/boot/KEFI/kernel"

def run():
    """
    Runs the KEFI VM via bhyve
    """
    args = parse_args()
    config_runner(args)
    if args.bhyve:
        create_b_vm()
        kgdb_pause()
        run_b_vm()
        cleanup_b_vm()
    elif args.qemu:
        create_q_vm()
        run_q_vm()
    else:
        sys.exit("ERROR: please supply a hypervisor option")

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--bhyve", help="create and run a bhyve VM",
                        action="store_true")
    parser.add_argument("-q", "--qemu", help="create and run a qemu VM",
                        action="store_true")
    return parser.parse_args()

def config_runner(args):
    """
    Configure the runner
    """
    global Q_MNT_PATH
    if os.getuid() != 0:
        exit("ERROR: root privileges required for virtualization")

    if args.qemu:
        if not os.path.isdir(Q_MNT_PATH):
            os.mkdir(Q_MNT_PATH)

    # load VMM
    os.system("sudo kldload vmm")

def kgdb_pause():
    """
    Give user time to setup kgdb instance
    """
    input("Setup KGDB now, press any key when ready to continue...")

def create_q_vm():
    """
    Create a kefi disk image for qemu
    """
    global DISK_PATH
    global Q_MNT_PATH
    global KEFI_KERN_PATH
    device = "/dev/md1s1"

    # Mount the disk image
    ret = os.system("sudo mdconfig -a -t vnode -f %s -u 1" % (DISK_PATH))
    if ret != 0:
        sys.exit("ERROR: mdconfig failed with error code: %d" % (os.WEXITSTATUS(ret)))

    os.system("sudo fsck -t ufs -y %s" % (device))

    ret = os.system("sudo mount %s %s" % (device, Q_MNT_PATH))
    if ret != 0:
        os.system("sudo mdconfig -d -u 1")
        sys.exit("ERROR: mount failed with error code: %d" % (os.WEXITSTATUS(ret)))

    # Copy the new kernel into the disk image
    new_kern_path = os.path.join(Q_MNT_PATH, "boot", "KEFI")
    if not os.path.isdir(new_kern_path):
        os.mkdir(new_kern_path)

    os.system("sudo cp %s %s" % (KEFI_KERN_PATH, os.path.join(new_kern_path, "kernel")))

    # Unmount the disk image
    os.system("sudo umount %s" % (Q_MNT_PATH))
    os.system("sudo mdconfig -d -u 1")

def run_q_vm():
    """
    Runs virtual machine in qemu
    """
    global MEM
    global DISK_PATH
    global NUM_VCPU

    cmd = "sudo qemu-system-x86_64 -net user -net nic -m %s -hda %s -smp %s " \
    "-curses -s -S" % (MEM, DISK_PATH, NUM_VCPU)

    ret = os.system(cmd)
    if ret != 0:
        sys.exit("ERROR: qemu failed with error code: %d" % (os.WEXITSTATUS(ret)))

def create_b_vm():
    """
    Creates a virual machine with bhyveload
    """
    global VM_NAME
    global CONS
    global MEM
    global DISK_PATH
    global KERNEL_PATH

    cmd = "sudo bhyveload -c %s -m %s -d %s -h %s %s" % (CONS, MEM, DISK_PATH,
                                                         KERNEL_PATH, VM_NAME)
    ret = os.system(cmd)
    if ret != 0:
        sys.exit("ERROR: bhyveload failed with error code: %d" % (os.WEXITSTATUS(ret)))

def run_b_vm():
    """
    Runs an existing virtual machine with bhyve
    """
    global MEM
    global NUM_VCPU
    global KGDB_PORT
    global VM_NAME
    global DISK_PATH

    cmd = "sudo bhyve -H -A -P -s 0:0,hostbridge -s 1:0,lpc -l com1,stdio " \
    "-s 2:0,virtio-net,tap0 -s 3:0,virtio-blk,%s " \
    "-m %s -c %s -G %s %s" % (DISK_PATH, MEM, NUM_VCPU, KGDB_PORT, VM_NAME)

    ret = os.system(cmd)
    if ret != 0:
        # cleanup the vm
        cleanup_b_vm()
        sys.exit("ERROR: bhyve failed with error code: %d" % (os.WEXITSTATUS(ret)))

def cleanup_b_vm():
    """
    Cleans up an old virtual machine with bhyvectl
    """
    global VM_NAME

    cmd = "sudo bhyvectl --vm=%s --destroy" % (VM_NAME)

    ret = os.system(cmd)
    if ret != 0:
        sys.exit("ERROR: bhyvectl failed with error code: %d" % (os.WEXITSTATUS(ret)))

if __name__ == "__main__":
    run()
